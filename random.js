const fetchRandomNumbers = () => {
    return new Promise((resolve, reject) => {
        console.log('Fetching number...')
        setTimeout(() => {
            let randomNum = Math.floor(Math.random() * (100 - 0 + 1)) + 0
            console.log('Received random number:', randomNum)
            resolve(randomNum)
        }, (Math.floor(Math.random() * (5)) + 1) * 1000)
    })
}

const fetchRandomString = () => {
    return new Promise((resolve, reject) => {
        console.log('Fetching string...')
        setTimeout(() => {
            let result = ''
            let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
            let charactersLength = characters.length
            for (let i = 0; i < 5; i++) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength))
            }
            console.log('Received random string:', result)
            resolve(result)
        }, (Math.floor(Math.random() * (5)) + 1) * 1000)
    })
}

function promiseTask1() {

    fetchRandomNumbers()
        .then((randomNumber) => {
            console.log(randomNumber)
        })
        .catch((error)=>{
            console.log(error)
        })

    fetchRandomString()
        .then((randomString) => {
            console.log(randomString)
        })
        .catch((error)=>{
            console.log(error)
        })
}
promiseTask1()

function promiseTask2() {

    Promise.all([fetchRandomNumbers(), fetchRandomNumbers()]).then((randomNumbersArray) => {
        console.log("\n\nFirst Random number=" + randomNumbersArray[0])
        console.log("Second Random number=" + randomNumbersArray[1])
        console.log("Therefore, Sum of 2 Random numbers:")
        console.log(randomNumbersArray[0] + randomNumbersArray[1])
    })
        .catch((error) => {
            console.log(error)
        })


}
promiseTask2()

function promiseTask3() {

    Promise.all([fetchRandomNumbers(), fetchRandomString()]).then(randomValuesArray => {
        let combinedText = randomValuesArray[1].concat('', randomValuesArray[0])
        console.log("The concatenated string =" + combinedText)
    })
        .catch((error) => {
            console.log(error)
        })
}
promiseTask3()

function promiseTask4() {
    let pendingPromiseArray = []
    for (let index = 0; index < 10; index++) {
        pendingPromiseArray.push(fetchRandomNumbers())
    }
    let sumOfRandomNumbers = 0
    Promise.all(pendingPromiseArray).then((resolvedPromiseArray) => {
        resolvedPromiseArray.forEach(index=> {
            sumOfRandomNumbers += index
        })
        console.log("The 10 Random numbers are: " + resolvedPromiseArray)
        console.log("Sum Of 10 Random Numbers= " + sumOfRandomNumbers)
    })
        .catch((error) => {
            console.log(error)
        })

}
promiseTask4()

async function task1() {
    try {
        const randomNumber = await fetchRandomNumbers()
        console.log("The system generated random number= " + randomNumber)

        const randomString = await fetchRandomString()
        console.log("The system generated random string= " + randomString)
    }
    catch (error) {
        console.log(error)
    }

}
task1()

async function task2() {
    try {
        const randomNumbersArray = await Promise.all([fetchRandomNumbers(), fetchRandomNumbers()])
        console.log("\n\nFirst Random number=" + randomNumbersArray[0])
        console.log("Second Random number=" + randomNumbersArray[1])
        console.log("Therefore, Sum of 2 Random numbers:")
        console.log(randomNumbersArray[0] + randomNumbersArray[1])
    }
    catch (error) {
        console.log(error)
    }
}
task2()

async function task3() {
    try {
        let randomValues = await Promise.all([fetchRandomNumbers(), fetchRandomString()])
        let combinedText = randomValues[1].concat('', randomValues[0])
        console.log("The concatenated string =" + combinedText)
    }
    catch (error) {
        console.log(error)
    }
}
task3()

async function task4() {
    try {
        let pendingPromiseArray = []
        for (let i = 0; i < 10; i++) {
            pendingPromiseArray.push(fetchRandomNumbers())
        }
        let sumOfRandomNumbers = 0
        let randomNumbers = await Promise.all(pendingPromiseArray)
        randomNumbers.forEach(index=>{
            sumOfRandomNumbers += index
        })
        console.log("\nThe 10 Random numbers are: " + randomNumbers)
        console.log("\nSum Of 10 Random Numbers= " + sumOfRandomNumbers)
    }
    catch (error) {
        console.log(error)
    }
}
task4()