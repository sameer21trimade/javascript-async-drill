function fetchData(data) {
    return new Promise(resolve => setTimeout(() => {
        resolve(data)
    }, 1000))
}

function sendUserLoginRequest(userId) {
    console.log(`Sending login request for ${userId}...`)
    return fetchData(userId).then(() => {
        console.log('Login successfull.', userId)
        return userId
    })
    .catch((error)=>{
        console.log(error)
    })
}

function getUserProfile(userId) {
    console.log(`Fetching profile for ${userId}...`)
    return fetchData({
        user1: { name: 'Vijay', points: 100 },
        user2: { name: 'Sahana', points: 200 }
    }).then(profiles => {
        console.log(`Received profile for ${userId}`)
        return profiles[userId]
    })
    .catch((error)=>{
        console.log(error)
    })
}

function getUserPosts(userId) {
    console.log(`Fetching posts for ${userId}...`)
    return fetchData({
        user1: [{ id: 1, title: 'Economics 101' }, { id: 2, title: 'How to negotiate' }],
        user2: [{ id: 3, title: 'CSS Animations' }, { id: 4, title: 'Understanding event loop' }]
    }).then(posts => {
        console.log(`Received posts for ${userId}`)
        return posts[userId]
    })
    .catch((error)=>{
        console.log(error)
    })
}

async function userDataSerial() {
    try {
        console.time('userData-serial')
        const loginRequest = await sendUserLoginRequest("user1")
        console.log(loginRequest)
        const userProfile = await getUserProfile("user1")
        console.log(userProfile)
        const postsData = await getUserPosts('user1')
        console.log(postsData)
        console.timeEnd('userData-serial')
    }
    catch (error) {
        console.log(error)
    }
}

async function userDataParallel() {
    try {
        console.time('userData-parallel')
        const userLogin = await sendUserLoginRequest("user1")
        console.log(userLogin)
        const userData = await Promise.all([getUserProfile("user1"), getUserPosts('user1')])
        console.log(userData)
        console.timeEnd('userData-parallel')
    }
    catch (error) {
        console.log(error)
    }

}

userDataSerial()
userDataParallel()