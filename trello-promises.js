function getBoard() {
  return new Promise((resolve, reject) => {
    console.log('Fetching board...')
    setTimeout(function () {
      let board = {
        id: "def453ed",
        name: "Thanos"
      }
      console.log('Received board')
      resolve(board)
    }, 1000)
  })
}

function getLists(boardId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching lists for board id ${boardId}...`)
    setTimeout(function () {
      let lists = {
        def453ed: [
          {
            id: "qwsa221",
            name: "Mind"
          },
          {
            id: "jwkh245",
            name: "Space"
          },
          {
            id: "azxs123",
            name: "Soul"
          },
          {
            id: "cffv432",
            name: "Time"
          },
          {
            id: "ghnb768",
            name: "Power"
          },
          {
            id: "isks839",
            name: "Reality"
          }
        ]
      }
      console.log(`Received lists for board id ${boardId}`)
      resolve(lists[boardId])
    }, 500)
  })
}

function getCards(listId) {
  return new Promise((resolve, reject) => {
    console.log(`Fetching cards for list id ${listId}...`)
    setTimeout(function () {
      let cards = {
        qwsa221: [
          {
            id: "1",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "2",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          },
          {
            id: "3",
            description: `Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar`
          }
        ],
        jwkh245: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "3",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "4",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        azxs123: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        cffv432: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ],
        ghnb768: [
          {
            id: "1",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          },
          {
            id: "2",
            description: `intercept a spaceship carrying the surviving Asgardians. As they extract the Space Stone from the Tesseract, Thanos subdues Thor, overpowers Hulk, and kills Heimdall and Loki.`
          }
        ]
      }
      console.log(`Received cards for list id ${listId}`)
      resolve(cards[listId] || [])
    }, 1500)
  })
}

function task1() {
  
  getBoard()
  .then(boardData => {
    return getLists(boardData.id)
  })
  .then( listsDataObjectsArray => {
    let eachListsData={}
    listsDataObjectsArray.forEach(index=>{      
      eachListsData[index.id]=index.name
    })
    for(let key in eachListsData)
    {
      if(key==='qwsa221')
      return key
    } 
  })
  .then((listID) => {
    getCards(listID)
      .then((cardsData) => {
        console.log(cardsData)
      })
      .catch((error)=>{
        console.log(error)
      })
  })
  .catch((error) => {
    console.log(error)
  })
}
task1()

function task2() {
  getBoard()
    .then((boardData) => {
      return getLists(boardData.id)
    })
    .then((listsDataObjectsArray) => {
      let pendingPromise = []          
      listsDataObjectsArray.forEach(index=>{
        if (index.id === 'qwsa221' || index.id === 'jwkh245')
          pendingPromise.push(getCards(index.id))
      })
      return pendingPromise
    })
    .then((pendingPromiseArray) => {
      Promise.all(pendingPromiseArray).then((cardsData) => {
        console.log(cardsData)
      })
      .catch((error)=>{
        console.log(error)
      })
    })
    .catch((error) => {
      console.log(error)
    })
}
task2()

function task3() {
  getBoard()
    .then((boardData) => {
      return getLists(boardData.id)
    })
    .then((listsDataObject) => {      
      let listsDataArray=Object.entries(listsDataObject)
      let pendingPromise = []
      listsDataArray.forEach(element=>{
        pendingPromise.push(getCards(element[1].id))
      })
      return pendingPromise
    })
    .then((pendingPromiseArray) => {
      Promise.all(pendingPromiseArray).then((cardsData) => {
        console.log(cardsData)
      })
      .catch((error)=>{
        console.log(error)
      })
    })
    .catch((error) => {
      console.log(error)
    })
}
task3()

